package anty

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAnty_Login(t *testing.T) {
	tests := []struct {
		name           string
		token          string
		statusCode     int
		responseString string
		err            error
	}{
		{
			name:       "Unauthorized",
			token:      "incorrect_token",
			statusCode: http.StatusUnauthorized,
			err:        ErrIncorrectAPIToken,
		},
		{
			name:       "Successful",
			token:      "correct_token",
			statusCode: http.StatusOK,
			err:        nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// Create a test server to mock the API response
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(test.statusCode)
			}))
			defer ts.Close()

			// Update the host variable to point to the test server
			host = ts.URL

			// Create a new Anty struct with the test token
			a := New(test.token)

			// Call the Login function and check the error
			err := a.Login()
			if err != test.err {
				t.Errorf("Expected error: %v, got: %v", test.err, err)
			}
		})
	}
}

func TestXxx(t *testing.T) {
	require := require.New(t)

	a := New("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTk2Mjk5MywidXNlcm5hbWUiOiJiZWxhbmVua290aW1vZmV5QGdtYWlsLmNvbSIsInJvbGUiOiJhZG1pbiIsInRlYW1JZCI6MTkwOTk4N30.EILxfJBdpY8gm1l6lWb3PEDDzVxWlmaQlbclf7JyiFI")
	profile, err := a.CreateDefaultProfile()
	require.NoError(err)
	require.NotNil(profile)
	v, ok := profile.Success.(float64)
	assert.True(t, ok)

	if v == 0 {
		t.Fail()
	}

	process, err := a.CreateBrowserProcess(profile)
	require.NoError(err)
	require.NotNil(profile)

	time.Sleep(time.Second)
	err = a.DeleteProfile(profile)
	require.NoError(err)

	_, _ = process, v

}
