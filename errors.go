package anty

import "github.com/pkg/errors"

var (
	ErrIncorrectAPIToken       = errors.Errorf("incorrect API token")
	ErrIncorrectStatusCode     = errors.Errorf("incorrect status code")
	ErrDefaultOptionsNotLoaded = errors.Errorf("default options not loaded")
)
