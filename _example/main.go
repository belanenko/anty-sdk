package main

import (
	"github.com/go-rod/rod"
	"github.com/rs/zerolog/log"
	"gitlab.com/belanenko/anty-sdk"
	_ "gitlab.com/belanenko/anty-sdk"
)

func main() {
	a := anty.New("token")
	profile, err := a.CreateDefaultProfile()
	if err != nil {
		log.Fatal().Err(err).Msg("can't create default profile")
	}

	process, err := a.CreateBrowserProcess(profile)
	if err != nil {
		log.Fatal().Err(err).Msg("can't create browser process")
	}

	err = a.DeleteProfile(profile)
	if err != nil {
		log.Fatal().Err(err).Msg("can't delete profile")
	}

	browser := rod.New()
	err = browser.ControlURL(process.ConnectionURL()).Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("can't connect to browser process")
	}
	defer browser.Close()

	browser.MustPages().First().Navigate("https://duckduckgo.com/")

}
