package anty

import (
	"bytes"
	"fmt"
	"io"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/belanenko/anty-sdk/profile"
)

var (
	host = "https://anty-api.com"

	endpointBrowserProfiles = "/browser_profiles"

	localhost                   = "http://localhost:3001"
	endpointStartBrowserProcess = "/v1.0/browser_profiles/%d/start?automation=1"
)

type authRoundTripper struct {
	BearerToken string
	transport   http.RoundTripper
}

func (a *authRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", a.BearerToken))
	return a.transport.RoundTrip(req)
}

type Anty struct {
	token      string
	httpclient *http.Client
}

func New(token string) *Anty {
	return &Anty{
		token: token,
	}
}

func (a *Anty) client() *http.Client {
	if a.httpclient != nil {
		return a.httpclient
	}
	transport := &authRoundTripper{
		BearerToken: a.token,
		transport:   http.DefaultTransport,
	}

	a.httpclient = &http.Client{Transport: transport}
	return a.httpclient
}

func (a *Anty) Login() error {
	req, err := http.NewRequest(http.MethodGet, host+endpointBrowserProfiles, nil)
	if err != nil {
		return err
	}

	client := a.client()
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusUnauthorized {
		return ErrIncorrectAPIToken
	}

	if resp.StatusCode != http.StatusOK {
		return ErrIncorrectStatusCode
	}

	return nil
}

func (a *Anty) CreateDefaultProfile() (*profile.Profile, error) {
	resp, err := a.client().Post(host+endpointBrowserProfiles, "application/json", bytes.NewReader(profile.DefaultOptions))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	p := &profile.Profile{}
	err = p.Unmarshal(b)

	return p, err
}

func (a *Anty) DeleteProfile(p *profile.Profile) error {
	req, err := http.NewRequest(http.MethodDelete, host+endpointBrowserProfiles+fmt.Sprintf("/%d", p.BrowserProfileID), nil)
	if err != nil {
		return err
	}
	resp, err := a.client().Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Debug().Msgf("DeleteProfile resp: '%s'", b)
	return nil
}

func (a *Anty) CreateBrowserProcess(p *profile.Profile) (*profile.Process, error) {
	resp, err := http.Get(localhost + fmt.Sprintf(endpointStartBrowserProcess, p.BrowserProfileID))
	if err != nil {
		//TODO: вынести ошибки в переменные
		return nil, err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		//TODO: вынести ошибки в переменные
		return nil, err
	}
	process := &profile.Process{}
	err = process.Unmarshal(b)

	return process, err
}
