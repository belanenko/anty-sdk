package profile

var DefaultOptions = []byte(`{
    "name": "anty-sdk",
    "tags": ["anty-sdk"],
    "platform": "macos",
    "browserType": "anty",
    "mainWebsite": "https://duckduckgo.com/",
	"useragent": {
		"mode": "manual",
		"value": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36"
	},
	"webrtc": {
		"mode": "altered",
		"ipAddress": null
	},
	"canvas": {
		"mode": "real"
	},
	"webgl": {
		"mode": "real"
	},
	"webglInfo": {
		"mode": "manual",
		"vendor": "ATI Technologies Inc.",
		"renderer": "AMD Radeon HD 6770M OpenGL Engine",
		"webgl2Maximum": "{\"MAX_SAMPLES\": 8, \"MAX_DRAW_BUFFERS\": 8, \"MAX_TEXTURE_SIZE\": 16384, \"MAX_ELEMENT_INDEX\": 4294967295, \"MAX_VIEWPORT_DIMS\": [16384, 16384], \"MAX_VERTEX_ATTRIBS\": 16, \"MAX_3D_TEXTURE_SIZE\": 16384, \"MAX_VARYING_VECTORS\": 32, \"MAX_ELEMENTS_INDICES\": 150000, \"MAX_TEXTURE_LOD_BIAS\": 16, \"MAX_COLOR_ATTACHMENTS\": 8, \"MAX_ELEMENTS_VERTICES\": 1048575, \"MAX_RENDERBUFFER_SIZE\": 16384, \"MAX_UNIFORM_BLOCK_SIZE\": 65536, \"MAX_VARYING_COMPONENTS\": 128, \"MAX_TEXTURE_IMAGE_UNITS\": 16, \"MAX_ARRAY_TEXTURE_LAYERS\": 2048, \"MAX_PROGRAM_TEXEL_OFFSET\": 7, \"MIN_PROGRAM_TEXEL_OFFSET\": -8, \"MAX_CUBE_MAP_TEXTURE_SIZE\": 16384, \"MAX_VERTEX_UNIFORM_BLOCKS\": 14, \"MAX_VERTEX_UNIFORM_VECTORS\": 768, \"MAX_COMBINED_UNIFORM_BLOCKS\": 70, \"MAX_FRAGMENT_UNIFORM_BLOCKS\": 14, \"MAX_UNIFORM_BUFFER_BINDINGS\": 70, \"MAX_FRAGMENT_UNIFORM_VECTORS\": 768, \"MAX_VERTEX_OUTPUT_COMPONENTS\": 128, \"MAX_FRAGMENT_INPUT_COMPONENTS\": 128, \"MAX_VERTEX_UNIFORM_COMPONENTS\": 3072, \"MAX_VERTEX_TEXTURE_IMAGE_UNITS\": 16, \"MAX_FRAGMENT_UNIFORM_COMPONENTS\": 3072, \"UNIFORM_BUFFER_OFFSET_ALIGNMENT\": 256, \"MAX_COMBINED_TEXTURE_IMAGE_UNITS\": 80, \"MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS\": 232448, \"MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS\": 4, \"MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS\": 232448, \"MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS\": 4, \"MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS\": 64}"
	},
    "clientRect": {
        "mode": "noise"
	},
    "timezone": {
      "mode": "auto"
    },
    "locale": {
        "mode": "auto"
	},
	"proxy": {
		"name": "anty-sdk",
		"host": "185.199.229.156",
		"port": "7492",
		"type": "http",
		"login": "untrxaih",
		"password": "kmff7s4ojnto"
	},
    "statusId": 0,
    "geolocation": {
      "mode": "auto",
      "latitude": null,
      "longitude": null,
      "accuracy": null
    },
    "cpu": {
      "mode": "manual",
      "value": 4
    },
    "memory": {
      "mode": "manual",
      "value": 8
    },
    "screen": {
      "mode": "real",
      "resolution": null
    },
    "audio": {
      "mode": "real"
    },
    "mediaDevices": {
      "mode": "real",
      "audioInputs": null,
      "videoInputs": null,
      "audioOutputs": null
    },
    "ports": {
      "mode": "protect",
      "blacklist": "3389,5900,5800,7070,6568,5938"
    },
    "doNotTrack": false,
    "args": [],
    "platformVersion": "10.15.7",
    "uaFullVersion": "107.0.5304.87",
    "login": "",
    "password": "",
    "appCodeName": "Mozilla",
    "platformName": "MacIntel",
    "connectionDownlink": 10,
    "connectionEffectiveType": "4g",
    "connectionRtt": 100,
    "connectionSaveData": 0,
    "cpuArchitecture": "",
    "osVersion": "10.15.7",
    "vendorSub": "",
    "productSub": "20030107",
    "vendor": "Google Inc.",
    "product": "Gecko"
  }`)
