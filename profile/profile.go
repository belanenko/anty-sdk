package profile

import "encoding/json"

type Profile struct {
	Success          interface{} `json:"success,omitempty"`
	BrowserProfileID int         `json:"browserProfileId,omitempty"`
	Data             *struct {
		ID int `json:"id,omitempty"`
	} `json:"data,omitempty"`
	Error *struct {
		Type string `json:"type"`
		Code string `json:"code"`
		Text string `json:"text"`
	} `json:"error,omitempty"`
}

func (p *Profile) Marshal() ([]byte, error) {
	return json.Marshal(p)
}

func (p *Profile) Unmarshal(data []byte) error {
	return json.Unmarshal(data, p)
}
