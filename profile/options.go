package profile

import "encoding/json"

type (
	Mode     string
	Platform string
)

const (
	Off     Mode = "off"
	Real    Mode = "real"
	Manual  Mode = "manual"
	Auto    Mode = "auto"
	Noise   Mode = "noise"
	Protect Mode = "protect"

	MacOS   Platform = "macos"
	Linux   Platform = "linux"
	Windows Platform = "windows"
)

type Options struct {
	Name                    string        `json:"name" validate:"required"`
	Tags                    []interface{} `json:"tags,omitempty"`
	Platform                Platform      `json:"platform" validate:"required"`
	BrowserType             string        `json:"browserType" validate:"required"`
	MainWebsite             string        `json:"mainWebsite"`
	Useragent               Useragent     `json:"useragent" validate:"required"`
	Webrtc                  Webrtc        `json:"webrtc" validate:"required"`
	Canvas                  Canvas        `json:"canvas" validate:"required"`
	Webgl                   Webgl         `json:"webgl" validate:"required"`
	WebglInfo               WebglInfo     `json:"webglInfo" validate:"required"`
	ClientRect              ClientRect    `json:"clientRect,omitempty"`
	Notes                   Notes         `json:"notes,omitempty"`
	Timezone                Timezone      `json:"timezone" validate:"required"`
	Locale                  Locale        `json:"locale" validate:"required"`
	Proxy                   *Proxy        `json:"proxy,omitempty"`
	StatusID                int           `json:"statusId,omitempty"`
	Geolocation             Geolocation   `json:"geolocation" validate:"required"`
	CPU                     CPU           `json:"cpu" validate:"required"`
	Memory                  Memory        `json:"memory" validate:"required"`
	Screen                  Screen        `json:"screen" validate:"required"`
	Audio                   Audio         `json:"audio" validate:"required"`
	MediaDevices            MediaDevices  `json:"mediaDevices" validate:"required"`
	Ports                   Ports         `json:"ports" validate:"required"`
	DoNotTrack              bool          `json:"doNotTrack,omitempty"`
	Args                    []interface{} `json:"args,omitempty"`
	PlatformVersion         string        `json:"platformVersion,omitempty"`
	UaFullVersion           string        `json:"uaFullVersion,omitempty"`
	Login                   string        `json:"login,omitempty"`
	Password                string        `json:"password,omitempty"`
	AppCodeName             string        `json:"appCodeName,omitempty"`
	PlatformName            string        `json:"platformName,omitempty"`
	ConnectionDownlink      int           `json:"connectionDownlink,omitempty"`
	ConnectionEffectiveType string        `json:"connectionEffectiveType,omitempty"`
	ConnectionRtt           int           `json:"connectionRtt,omitempty"`
	ConnectionSaveData      int           `json:"connectionSaveData,omitempty"`
	CPUArchitecture         string        `json:"cpuArchitecture,omitempty"`
	OsVersion               string        `json:"osVersion,omitempty"`
	VendorSub               string        `json:"vendorSub,omitempty"`
	ProductSub              string        `json:"productSub,omitempty"`
	Vendor                  string        `json:"vendor,omitempty"`
	Product                 string        `json:"product,omitempty"`
}

func (o *Options) Marshal() ([]byte, error) {
	return json.Marshal(o)
}

func (o *Options) Unmarshal(data []byte) error {
	return json.Unmarshal(data, o)
}

type Useragent struct {
	Mode  Mode   `json:"mode"`
	Value string `json:"value,omitempty"`
}
type Webrtc struct {
	Mode      Mode        `json:"mode"`
	IPAddress interface{} `json:"ipAddress,omitempty"`
}
type Canvas struct {
	Mode Mode `json:"mode"`
}
type Webgl struct {
	Mode Mode `json:"mode"`
}
type WebglInfo struct {
	Mode          Mode   `json:"mode"`
	Vendor        string `json:"vendor,omitempty"`
	Renderer      string `json:"renderer,omitempty"`
	Webgl2Maximum string `json:"webgl2Maximum,omitempty"`
}
type ClientRect struct {
	Mode Mode `json:"mode"`
}
type Notes struct {
	Content interface{} `json:"content"`
	Color   string      `json:"color"`
	Style   string      `json:"style"`
	Icon    string      `json:"icon"`
}
type Timezone struct {
	Mode  Mode        `json:"mode"`
	Value interface{} `json:"value,omitempty"`
}
type Locale struct {
	Mode  Mode        `json:"mode"`
	Value interface{} `json:"value,omitempty"`
}
type Proxy struct {
	Name     string `json:"name"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	Type     string `json:"type"`
	Login    string `json:"login,omitempty"`
	Password string `json:"password,omitempty"`
}
type Geolocation struct {
	Mode      Mode        `json:"mode"`
	Latitude  interface{} `json:"latitude,omitempty"`
	Longitude interface{} `json:"longitude,omitempty"`
	Accuracy  interface{} `json:"accuracy,omitempty"`
}
type CPU struct {
	Mode  Mode `json:"mode"`
	Value int  `json:"value,omitempty"`
}
type Memory struct {
	Mode  Mode `json:"mode"`
	Value int  `json:"value,omitempty"`
}
type Screen struct {
	Mode       string      `json:"mode"`
	Resolution interface{} `json:"resolution,omitempty"`
}
type Audio struct {
	Mode Mode `json:"mode"`
}
type MediaDevices struct {
	Mode         Mode        `json:"mode"`
	AudioInputs  interface{} `json:"audioInputs,omitempty"`
	VideoInputs  interface{} `json:"videoInputs,omitempty"`
	AudioOutputs interface{} `json:"audioOutputs,omitempty"`
}
type Ports struct {
	Mode      Mode   `json:"mode"`
	Blacklist string `json:"blacklist,omitempty"`
}
