package profile

import (
	"encoding/json"
	"fmt"
)

type Process struct {
	Success    bool `json:"success"`
	Automation struct {
		Port       int    `json:"port"`
		WsEndpoint string `json:"wsEndpoint"`
	} `json:"automation"`
	Error       string `json:"error"`
	ErrorObject struct {
		Message string `json:"message"`
		Name    string `json:"name"`
		Stack   string `json:"stack"`
		Code    string `json:"code"`
		Status  int    `json:"status"`
	} `json:"errorObject"`
}

func (p *Process) ConnectionURL() string {
	return fmt.Sprintf("ws://127.0.0.1:%d%s", p.Automation.Port, p.Automation.WsEndpoint)
}

func (p *Process) Marshal() ([]byte, error) {
	return json.Marshal(p)
}

func (p *Process) Unmarshal(data []byte) error {
	return json.Unmarshal(data, p)
}
